var mysqlModel = require('mysql-model');
var requestIp = require('request-ip');
var dateTime = require('node-datetime');
var url = require('url') ;
var Employee = require('../models/employee');


var MyAppModel = mysqlModel.createConnection({
  host     : 'localhost',
  user     : 'root',
  database : 'sst_employee',
});
 
var Employee = MyAppModel.extend({
    tableName: "employee",
});


 
student = new Employee();

exports.getEmployeeServer3 = function(req,res){
     var clientIp = requestIp.getClientIp(req);
     var dt = dateTime.create();
     var formatted = dt.format('Y-m-d H:M:S');
    
     var hostname = req.headers.host; // hostname = 'localhost:8080'
     var pathname = url.parse(req.url).pathname; // pathname = '/MyApp'  
    
   
     
    student.find('all', {where: "is_active = 1"}, function(err, rows, fields) {
           if (!err){
               var status = '200';
               var message = 'Successful';
               res.json(rows)
               /* console.log('SST Record at Server Time '+formatted, rows);*/
          
           }else{
                var message = err;
                var status = '500'; 
                }
        
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: GET');
        console.log('Response : Status '+status+' | Message '+message);
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
        
        
     });
    
};