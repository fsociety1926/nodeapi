var Employee = require('../models/employee');
var mysqlModel = require('mysql-model');
var dateTime = require('node-datetime');
var requestIp = require('request-ip');
var url = require('url') ;


var MyAppModel = mysqlModel.createConnection({ //Set-Up Connection to MYSQLDB
  host     : 'localhost',
  user     : 'root',
  database : 'sst_employee'
});
 
var SSTEmployee = MyAppModel.extend({ //Init Model Table Name
    tableName: "employee",
});



// Create endpoint /api/employee for POST
exports.postEmployee = function(req, res) {
  // Create a new instance of the employee model
  
  var employee = new Employee();
  
  var dt = dateTime.create();    
  var formatted = dt.format('Y-m-d H:M:S');  
  var hostname = req.headers.host; // base url
  var pathname = url.parse(req.url).pathname; // pathname  
  var clientIp = requestIp.getClientIp(req);  
    
 
  // Set the employee properties that came from the POST data
  employee.emp_firstname = req.body.first_name;
  employee.emp_middlename = req.body.middle_name;
  employee.emp_lastname = req.body.last_name;
  employee.emp_email = req.body.email;
  employee.emp_is_active = 1;

  // Save the employee and check for errors
  employee.save(function(err) { //Mongo DB
    if (err){
            var message = err;
            var status = '500'; 
           return res.send(err);
   
    }else{
            student = new SSTEmployee({
                first_name: ''+req.body.first_name+'',
                middle_name: ''+req.body.middle_name+'',
                last_name: ''+req.body.last_name+'',
                create_at: ''+formatted+'',
                email_address: ''+req.body.email+'',
                emp_hashid: ''+employee._id+''
               
             });
             
             student.save(); //save to mysql db
           
             var status = '200';
             var message = 'Successful';
       
          
        res.json({ message: 'Successfuly added new employee', data: employee });
    }
      
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: POST');
        console.log('Response : Status '+status+' | Message '+message);
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
  });
    
    
   
    
    
};


//Get all employee details
exports.getEmployee = function(req,res){
  
     var clientIp = requestIp.getClientIp(req);
     var dt = dateTime.create();
     var formatted = dt.format('Y-m-d H:M:S');
    
     var hostname = req.headers.host; // hostname = 'localhost:8080'
     var pathname = url.parse(req.url).pathname; // pathname = '/MyApp'  
    
  Employee.find(function(err, employees) {
    if(err){ 
         var message = err;
         var status = '500'; 
         
         return res.send(err)
     }else{
         var status = '200';
         var message = 'Successful';
         
         res.json(employees);
     }
      
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: GET');
        console.log('Response : Status '+status+' | Message '+message);
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
      
  });
    
};


// Create endpoint /api/employee/:empid for PUT
exports.putEmployee = function(req, res) {
  // Use the Employee model to update a specific employee details
    
      var dt = dateTime.create();    
      var formatted = dt.format('Y-m-d H:M:S');  
      var hostname = req.headers.host; // base url
      var pathname = url.parse(req.url).pathname; // pathname  
      var clientIp = requestIp.getClientIp(req);  
    
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: PUT');
    
  Employee.update({_id: req.params.empid},{$set:{emp_email:req.body.email}},{multi:true},function(err, num, raw){
       
      
       if (err){
            var message = err;
            var status = '500'; 
            return res.send(err); //return when error occure upon updating employee
            console.log('Response : Status '+status+' | Message '+message);
       }else{
      
           Employee.findById({ _id: req.params.empid }, function(err,employee) { //return with new employee details
               if (err){
                  var message = err;
                  var status = '500';   
                  return res.send(err); 
               }else{

                 student = new SSTEmployee({
                      email_address: ''+req.body.email+'',
                      emp_hashid: ''+req.params.empid+''
                 });

                 
                 student.set('emp_id',''+req.params.empid+'');
                 student.save();

                   var status = '200';
                   var message = 'Successful';     
                   
                 res.json(employee);  
               }
              
                console.log('Response : Status '+status+' | Message '+message);
               
           });
          
        }
       
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
      
      
      
  });  
};


// Create endpoint /api/employee/:empid for DELETE
exports.deleteEmployee = function(req, res) {
  // Use the Employee model to delete a specific employee details
    
      var dt = dateTime.create();    
      var formatted = dt.format('Y-m-d H:M:S');  
      var hostname = req.headers.host; // base url
      var pathname = url.parse(req.url).pathname; // pathname  
      var clientIp = requestIp.getClientIp(req);
    
  Employee.remove({_id: req.params.empid},function(err, num, raw){
       if(err){
            var message = err;
            var status = '500'; 
           res.send(err);//return when error occure upon updating employee
       }else{
           
         student = new SSTEmployee();
         student.set('emp_id',''+req.params.empid+'');
         student.remove();
           
         var status = '200';
         var message = 'Successful';    
         
         res.json('Successfuly Remove');      
       }
      
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: DELETE');
        console.log('Response : Status '+status+' | Message '+message);
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
        
  });  
};


//create endpoint for single data query from employee table
exports.getEmployees = function(req,res){
    
      var dt = dateTime.create();    
      var formatted = dt.format('Y-m-d H:M:S');  
      var hostname = req.headers.host; // base url
      var pathname = url.parse(req.url).pathname; // pathname  
      var clientIp = requestIp.getClientIp(req);
    
    Employee.findById({_id:req.params.empid},function(err,employee){
       if(err){
           var status = '200';
           var message = 'Successful';
           res.send(err)
       }else{
           var status = '200';
           var message = 'Successful';
           res.json(employee);
       }
        
        console.log('\n');
        console.log('Getting Request from Host: '+ clientIp.replace("::ffff:",""));
        console.log('Method Access: GET');
        console.log('Response : Status '+status+' | Message '+message);
        console.log('URL Access: http://' + hostname + pathname);
        console.log('Server Time: '+formatted);
        
    });
    
};

