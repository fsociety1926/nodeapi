var express = require('express')
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var passport = require('passport');
var session = require('express-session');
var cors_proxy = require('cors-anywhere');
var mysql = require('mysql');
var schedule = require('node-schedule');
var dateTime = require('node-datetime');
var url = require('url') ;


//Controller
var staticserverController = require('./controllers/staticserver');
var userController = require('./controllers/user');
var authController = require('./controllers/auth');
var oauth2Controller = require('./controllers/oauth2');
var clientController = require('./controllers/client');
var beerController = require('./controllers/beer');
var employeeController = require('./controllers/employee');
var dataserverController = require('./controllers/dataserver');


/*

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  database : 'sst_employee'
});*/


// Connect to the beerlocker MongoDB
mongoose.Promise = global.Promise; 
mongoose.connect('mongodb://localhost:27017/sst_nodedb');


var app = express();

//var rule = new schedule.RecurrenceRule();
//rule.second = [0, 20, 40];
//
//var j = schedule.scheduleJob(rule, function(){
//        dataserverController.getEmployeeServer2();
//});



// Set view engine to ejs template
app.set('view engine', 'ejs');

// Use the body-parser package in our application
app.use(bodyParser.json());

app.use(function(req,res,next){ //CORE configuration to allow all origin request
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    res.header("Access-Control-Allow-Methods","GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
})

app.use(bodyParser.urlencoded({
  extended: true
}));

// Use express session support since OAuth2orize requires it
app.use(session({
  secret: 'Super Secret Session Key', // Set your secret word
  saveUninitialized: true,
  resave: true
}));

// Use the passport package in our application
app.use(passport.initialize());

// Create our Express router
var router = express.Router();


// Create endpoint handlers for employee



router.route('/employee')
  .post(authController.isAuthenticated, employeeController.postEmployee) //Creat new employee
  .get(authController.isAuthenticated, employeeController.getEmployee); //Get all employee
 
 
router.route('/employee/:empid')
  .get(authController.isAuthenticated, employeeController.getEmployees) //Get specific employee  
  .put(authController.isAuthenticated, employeeController.putEmployee) // Update employee first name
  .delete(authController.isAuthenticated, employeeController.deleteEmployee); // Delete employee

// Create endpoint handlers for oauth2 authorize
router.route('/oauth2/authorize') // Generate code (code is require for generating access token)
  .get(authController.isAuthenticated, oauth2Controller.authorization)
  .post(authController.isAuthenticated, oauth2Controller.decision);


router.route('/oauth2/token')// Generate access token, require for every accessing the method 
  .post(authController.isClientAuthenticated, oauth2Controller.token,function(req,res,next){
    
})
// Register all our routes with "api" prefix
app.use('/api', router);

var server = app.listen(5000,function(){ //Listen to port 5000
    console.log('SST Server Start Up at', server.address().port)
})

/*router.route('/serveremployee')
   .get(authController.isAuthenticated, dataserverController.getEmployeeServer2);
   .get(authController.isAuthenticated, staticserverController.getEmployeeServer3);*/


// Start the server
/*app.listen(5000);*/


/*var host = 'localhost';
cors_proxy.createServer().listen(5000,host,function() {
    console.log('CORS proxy running on ');
})*/


/*// Create endpoint handlers for /beers
router.route('/beers')
  .post(authController.isAuthenticated, beerController.postBeers)
  .get(authController.isAuthenticated, beerController.getBeers);

// Create endpoint handlers for /beers/:beer_id
router.route('/beers/:beer_id')
  .get(authController.isAuthenticated, beerController.getBeer)
  .put(authController.isAuthenticated, beerController.putBeer)
  .delete(authController.isAuthenticated, beerController.deleteBeer);

// Create endpoint handlers for /users
router.route('/users')
  .post(userController.postUsers)
  .get(authController.isAuthenticated, userController.getUsers);

// Create endpoint handlers for /clients
router.route('/clients')
  .post(authController.isAuthenticated, clientController.postClients)
  .get(authController.isAuthenticated, clientController.getClients);*/


// Create endpoint handlers for oauth2 token
/*router.route('/oauth2/token')
  .post(authController.isClientAuthenticated, oauth2Controller.token);*/