// Load required packages
var mongoose = require('mongoose');
mongoose.Promise = global.Promise; 

// Define our token schema
var EmployeeSchema   = new mongoose.Schema({
  emp_firstname: { type: String, required: true },
  emp_middlename: { type: String },
  emp_lastname: { type: String, required: true },
  emp_is_active: { type: Boolean },
  emp_email:{type: String}    
});

// Export the Mongoose model
module.exports = mongoose.model('Employee', EmployeeSchema);